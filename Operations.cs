﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Lab1
{
    public enum OperationOptions { Cut, Copy, Delete };

    static class Operations
    {
        public static event EventHandler OnOneFileProcessed;
        private static System.Collections.Specialized.StringCollection filesToCutOrCopy = new System.Collections.Specialized.StringCollection();
        private static OperationOptions cutOrCopy;

        public static int FilesToPasteCount
        {
            get { return filesToCutOrCopy.Count; }
        }

        public static void Rename(string oldName, string newName)
        {
            string oldPath = @oldName;
            string newPath = Path.Combine(oldName.Remove(oldName.Length - Path.GetFileName(oldName).Length), @newName);
            bool isDirectory = (File.GetAttributes(oldPath) & FileAttributes.Directory) == FileAttributes.Directory;

            if (oldPath == newPath)
                return;

            if (string.IsNullOrEmpty(newName))
                throw new ArgumentException("New name can't be empty.");
            foreach (char invalidChar in Path.GetInvalidFileNameChars())
                if (newName.Contains(invalidChar.ToString()))
                    throw new ArgumentException("Invalid char in new name.");
            if (isDirectory && Directory.Exists(newPath))
                throw new ArgumentException("Directory with such name already exists.");
            else if (!isDirectory && File.Exists(newPath))
                throw new ArgumentException("File with such name already exists.");

            if (isDirectory)
                Directory.Move(oldPath, newPath);
            else
                File.Move(oldPath, newPath);
        }

        public static void CreateDirectory(string where, string name)
        {
            string newPath = Path.Combine(where, name);

            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("New name can't be empty.");
            foreach (char invalidChar in Path.GetInvalidPathChars())
                if (name.Contains(invalidChar.ToString()))
                    throw new ArgumentException("Invalid char in new name.");
            if (Directory.Exists(newPath))
                throw new ArgumentException("Directory with such name already exists.");

            Directory.CreateDirectory(newPath);
        }

        public static void Delete(string path)
        {
            bool isDirectory = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;
            if (isDirectory)
                Directory.Delete(path, true);
            else
                File.Delete(path);
        }

        public static void ResetOperation()
        {
            filesToCutOrCopy.Clear();
        }

        public static void CutOrCopy(string path, OperationOptions option)
        {
            filesToCutOrCopy.Add(path);
            cutOrCopy = option;
        }

        public static void Paste(string destination)
        {
            if (filesToCutOrCopy.Count == 0)
                return;


            foreach (string path in filesToCutOrCopy)
            {
                try
                {
                    if (!File.Exists(path) && !Directory.Exists(path))
                        throw new ArgumentException(@path + "\nFile not found.");
                    bool isDirectory = (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;
                    if (!isDirectory && (File.Exists(Path.Combine(destination, Path.GetFileName(path)))))
                        throw new ArgumentException(@path + "\nFile with such name already exists.");
                    if (isDirectory && (Directory.Exists(Path.Combine(destination, Path.GetFileName(path)))))
                        throw new ArgumentException(@path + "\nDirectory with such name already exists.");


                    if (!isDirectory)
                        File.Copy(path, Path.Combine(destination, Path.GetFileName(path)), false);
                    else if (isDirectory)
                    {
                        Directory.CreateDirectory(Path.Combine(destination, Path.GetFileName(path)));
                        CopyDirectory(path, Path.Combine(destination, Path.GetFileName(path)));
                    }

                    if (cutOrCopy == OperationOptions.Cut)
                        Delete(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
                finally
                {
                    if (OnOneFileProcessed != null)
                        OnOneFileProcessed(path, EventArgs.Empty);
                }
            }
            ResetOperation();
        }

        private static void CopyDirectory(string sourceDir, string targetDir)
        {
            Directory.CreateDirectory(targetDir);

            foreach (string file in Directory.GetFiles(sourceDir))
            {
                string newName = Path.Combine(targetDir, Path.GetFileName(file));
                try
                {
                    if (File.Exists(newName))
                        throw new ArgumentException(@newName + "\nFile with such name already exists.");
                    File.Copy(file, newName, false);
                }
                catch (Exception) { continue; }
            }

            foreach (string directory in Directory.GetDirectories(sourceDir))
            {
                string newName = Path.Combine(targetDir, Path.GetFileName(directory));
                try
                {
                    if (Directory.Exists(newName))
                        throw new ArgumentException(@newName + "\nDirectory with such name already exists.");
                }
                catch (Exception) { continue; }

                CopyDirectory(directory, newName);
            }
        }
    }
}
