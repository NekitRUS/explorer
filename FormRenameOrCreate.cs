﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Lab1
{
    public enum FormOptions { Create, Rename };

    public partial class FormRenameOrCreate: Form
    {
        public FormRenameOrCreate(FormOptions options, string parentDirectoryNameOrOldName)
        {
            InitializeComponent();
            textBoxName.Tag = parentDirectoryNameOrOldName;
            if (options == FormOptions.Create)
                this.Text = "New folder";
            else
            {
                this.Text = "Rename";
                textBoxName.Text = Path.GetFileName(parentDirectoryNameOrOldName);
                textBoxName.SelectAll();
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.Text == "Rename")
                Operations.Rename((string)textBoxName.Tag, textBoxName.Text);
            else
                Operations.CreateDirectory((string)textBoxName.Tag, textBoxName.Text);
            this.Close();
        }
    }
}
