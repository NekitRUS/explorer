﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Lab1
{
    public partial class FormProperties: Form
    {
        public FormProperties(FileSystemInfo itemInformation)
        {
            InitializeComponent();
            this.Text = "Properties: " + itemInformation.Name;

            ListViewItem newItem;
            ListViewItem.ListViewSubItem newSubItem;

            newItem = new ListViewItem { Text = "Name" };
            newSubItem = new ListViewItem.ListViewSubItem { Text = itemInformation.Name };
            newItem.SubItems.Add(newSubItem);
            listViewProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Type" };
            if (itemInformation is FileInfo)
                newSubItem = new ListViewItem.ListViewSubItem { Text = itemInformation.Extension };
            else
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Folder" };
            newItem.SubItems.Add(newSubItem);
            listViewProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Full path" };
            newSubItem = new ListViewItem.ListViewSubItem { Text = itemInformation.FullName };
            newItem.SubItems.Add(newSubItem);
            listViewProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Full size" };
            long fullSize;
            if (itemInformation is FileInfo)
                fullSize = ((FileInfo)itemInformation).Length;
            else
                fullSize = DirectorySize(itemInformation as DirectoryInfo);
            newSubItem = new ListViewItem.ListViewSubItem { Text = string.Format("{0:#,#;#,#;0}", fullSize) + " bytes" };
            newItem.SubItems.Add(newSubItem);
            listViewProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Create time" };
            newSubItem = new ListViewItem.ListViewSubItem { Text = string.Format("{0:G}", itemInformation.CreationTime) };
            newItem.SubItems.Add(newSubItem);
            listViewProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Last modified" };
            newSubItem = new ListViewItem.ListViewSubItem { Text = string.Format("{0:G}", itemInformation.LastWriteTime) };
            newItem.SubItems.Add(newSubItem);
            listViewProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Hidden" };
            if ((itemInformation.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Yes" };
            else
                newSubItem = new ListViewItem.ListViewSubItem { Text = "No" };
            newItem.SubItems.Add(newSubItem);
            listViewProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "Read only" };
            if ((itemInformation.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Yes" };
            else
                newSubItem = new ListViewItem.ListViewSubItem { Text = "No" };
            newItem.SubItems.Add(newSubItem);
            listViewProperties.Items.Add(newItem);

            newItem = new ListViewItem { Text = "System" };
            if ((itemInformation.Attributes & FileAttributes.System) == FileAttributes.System)
                newSubItem = new ListViewItem.ListViewSubItem { Text = "Yes" };
            else
                newSubItem = new ListViewItem.ListViewSubItem { Text = "No" };
            newItem.SubItems.Add(newSubItem);
            listViewProperties.Items.Add(newItem);
        }

        private long DirectorySize(DirectoryInfo name)
        {
            long totalSize = 0;
            foreach (FileInfo file in name.GetFiles())
            {
                try { totalSize += file.Length; }
                catch { continue; }
            }
            foreach (DirectoryInfo dir in name.GetDirectories())
            {
                try { totalSize += DirectorySize(dir); }
                catch { continue; }
            }
            return totalSize;
        }
    }
}
