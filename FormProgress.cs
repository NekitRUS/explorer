﻿using System;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Lab1
{
    public partial class FormProgress: Form
    {
        private ListViewItem[] items;
        private OperationOptions operations;
        private string path;
        private bool firstLoad = true;

        public FormProgress(string caption, ListViewItem[] items = null, OperationOptions operation = OperationOptions.Copy, string destination = "")
        {
            InitializeComponent();
            this.Text = caption;
            this.operations = operation;
            this.items = items;
            this.path = destination;

            Operations.OnOneFileProcessed += Operations_OneFileProcessed;

            if (operations == OperationOptions.Delete)
                this.progressBarProgress.Maximum = this.items.Length;
            else
                this.progressBarProgress.Maximum = Operations.FilesToPasteCount;
        }

        void Operations_OneFileProcessed(object sender, EventArgs e)
        {
            if (!InvokeRequired)
                progressBarProgress.Increment(1);
            else
                Invoke(new Action(() => progressBarProgress.Increment(1)));
        }

        private void FormProgress_Paint(object sender, PaintEventArgs e)
        {
            if (firstLoad)
            {
                Task.Factory.StartNew(new Action(() =>
                {
                    if (operations == OperationOptions.Delete)
                    {
                        foreach (ListViewItem item in this.items)
                        {
                            try
                            {
                                Operations.Delete((string)item.Tag);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                continue;
                            }
                            finally { Operations_OneFileProcessed((string)item.Tag, EventArgs.Empty); }
                        }
                    }
                    else
                        Operations.Paste(path);

                    this.DialogResult = DialogResult.OK;

                }));
            }
            firstLoad = false;
        }
    }
}
