﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace Lab1
{
    class ListViewItemComparer: IComparer
    {
        private int columnIndex;
        private SortOrder order;

        public ListViewItemComparer()
        {
            columnIndex = 0;
            order = SortOrder.Ascending;
        }
        public ListViewItemComparer(int column, SortOrder order)
        {
            columnIndex = column;
            this.order = order;
        }
        public int Compare(object x, object y)
        {
            int returnVal = -1;

            if ((columnIndex == 3) && !string.IsNullOrEmpty(((ListViewItem)x).SubItems[columnIndex].Text) && !string.IsNullOrEmpty(((ListViewItem)y).SubItems[columnIndex].Text))
            {
                string separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;
                returnVal = Int32.Parse(((ListViewItem)x).SubItems[columnIndex].Text.Replace(separator, "")) - Int32.Parse(((ListViewItem)y).SubItems[columnIndex].Text.Replace(separator, ""));
                if (order == SortOrder.Descending)
                    returnVal *= -1;
            }
            else if ((columnIndex == 0) && (((ListViewItem)x).ImageKey == "folder") && (((ListViewItem)y).ImageKey != "folder"))
                returnVal = -1;
            else if ((columnIndex == 0) && (((ListViewItem)x).ImageKey != "folder") && (((ListViewItem)y).ImageKey == "folder"))
                returnVal = 1;
            else if ((((ListViewItem)x).ImageKey == "folder") && (((ListViewItem)y).ImageKey != "folder"))
                returnVal = -1;
            else if ((((ListViewItem)x).ImageKey != "folder") && (((ListViewItem)y).ImageKey == "folder"))
                returnVal = 1;
            else
            {
                returnVal = String.Compare(((ListViewItem)x).SubItems[columnIndex].Text, ((ListViewItem)y).SubItems[columnIndex].Text);
                if (order == SortOrder.Descending)
                    returnVal *= -1;
            }

            return returnVal;
        }
    }

}
