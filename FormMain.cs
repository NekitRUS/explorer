﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lab1
{
    public partial class FormMain: Form
    {
        #region Variables Declaration
        private int sortColumnNumber = -1;
        private DirectoryInfo curDir;
        private string toolTipPopUpText = "";
        #endregion

        public FormMain()
        {
            InitializeComponent();
            curDir = new DirectoryInfo(Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System)));
            CreateHeaders();
            FillDrives(new DriveInfo(curDir.Root.FullName));
            FillListView(curDir);
        }

        #region Methods
        private void FillDrives(DriveInfo startDrive)
        {
            toolStripComboBoxDrives.Items.Clear();

            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                toolStripComboBoxDrives.Items.Add(drive.Name);
            }

            toolStripComboBoxDrives.SelectedItem = @startDrive.Name;
        }

        private void CreateHeaders()
        {
            ColumnHeader columns;

            columns = new ColumnHeader { Text = "File Name", Width = 280, TextAlign = HorizontalAlignment.Center };
            listViewBrowser.Columns.Add(columns);

            columns = new ColumnHeader { Text = "Type", Width = 100, TextAlign = HorizontalAlignment.Center };
            listViewBrowser.Columns.Add(columns);

            columns = new ColumnHeader { Text = "Create Time", Width = 120, TextAlign = HorizontalAlignment.Center };
            listViewBrowser.Columns.Add(columns);

            columns = new ColumnHeader { Text = "Size, KB", Width = 140, TextAlign = HorizontalAlignment.Center };
            listViewBrowser.Columns.Add(columns);
        }

        private void FillListView(DirectoryInfo directory)
        {
            ListViewItem item;
            ListViewItem.ListViewSubItem subItem;
            FileAttributes attributes;

            try
            {
                if (string.IsNullOrEmpty(directory.FullName))
                    return;

                listViewBrowser.BeginUpdate();
                listViewBrowser.Items.Clear();
                imageList.Images.Clear();

                if (!imageList.Images.ContainsKey("folder"))
                    imageList.Images.Add("folder", global::Lab1.Properties.Resources.defaultFolder);
                foreach (DirectoryInfo dir in directory.GetDirectories())
                {
                    attributes = dir.Attributes;
                    if ((((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && this.hiddenFilesToolStripMenuItem.Checked) && !((attributes & FileAttributes.System) == FileAttributes.System) ||
                       (((attributes & FileAttributes.System) == FileAttributes.System) && this.systemToolStripMenuItem.Checked) && !((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) ||
                       (((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && this.hiddenFilesToolStripMenuItem.Checked && ((attributes & FileAttributes.System) == FileAttributes.System) && this.systemToolStripMenuItem.Checked) ||
                       (!((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && !((attributes & FileAttributes.System) == FileAttributes.System)))
                    {
                        item = new ListViewItem { Text = dir.Name, Tag = dir.FullName, ImageKey = "folder" };

                        subItem = new ListViewItem.ListViewSubItem { Text = "" };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = "" };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = "" };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = "" };
                        item.SubItems.Add(subItem);

                        listViewBrowser.Items.Add(item);
                    }
                }
                int foldersCount = listViewBrowser.Items.Count;

                foreach (FileInfo file in directory.GetFiles())
                {
                    attributes = file.Attributes;
                    if ((((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && this.hiddenFilesToolStripMenuItem.Checked) && !((attributes & FileAttributes.System) == FileAttributes.System) ||
                       (((attributes & FileAttributes.System) == FileAttributes.System) && this.systemToolStripMenuItem.Checked) && !((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) ||
                       (((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && this.hiddenFilesToolStripMenuItem.Checked && ((attributes & FileAttributes.System) == FileAttributes.System) && this.systemToolStripMenuItem.Checked) ||
                       (!((attributes & FileAttributes.Hidden) == FileAttributes.Hidden) && !((attributes & FileAttributes.System) == FileAttributes.System)))
                    {
                        System.Drawing.Icon iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName);

                        if (!imageList.Images.ContainsKey(file.FullName))
                            imageList.Images.Add(file.FullName, iconForFile);
                        else
                        {
                            imageList.Images.RemoveByKey(file.FullName);
                            imageList.Images.Add(file.FullName, iconForFile);
                        }

                        item = new ListViewItem { Text = file.Name.Substring(0, file.Name.Length - file.Extension.Length), Tag = file.FullName, ImageKey = file.FullName };

                        subItem = new ListViewItem.ListViewSubItem { Text = file.Extension };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = file.CreationTime.ToString() };
                        item.SubItems.Add(subItem);

                        subItem = new ListViewItem.ListViewSubItem { Text = string.Format("{0:#,#;#,#;0}", file.Length / 1024) };
                        item.SubItems.Add(subItem);

                        listViewBrowser.Items.Add(item);
                    }
                }

                toolStripStatusLabelPath.Text = toolStripStatusLabelPath.ToolTipText = directory.FullName;

                toolStripStatusLabelCounts.Text = toolStripStatusLabelCounts.ToolTipText = "Folders: " + foldersCount + " Files: " + (listViewBrowser.Items.Count - foldersCount);

                DriveInfo drive = new DriveInfo(directory.Root.FullName);
                string driveInformation = "<" + @drive.Name + ">" + drive.DriveFormat +
                        string.Format(" Free: {0:f2}GB of {1:f2}GB total", ((double)(drive.TotalFreeSpace / 1073741824.0)), ((double)(drive.TotalSize / 1073741824.0)));
                toolStripStatusLabelDrive.Text = toolStripStatusLabelDrive.ToolTipText = driveInformation;

                toolStripProgressBarDrive.Value = (int)((drive.TotalSize - drive.TotalFreeSpace) / (double)drive.TotalSize * 100);
                if (toolStripProgressBarDrive.Value < 60)
                    toolStripProgressBarDrive.SetColor(ProgressBarExtension.ProgressBarColor.Green);
                else if (toolStripProgressBarDrive.Value < 90)
                    toolStripProgressBarDrive.SetColor(ProgressBarExtension.ProgressBarColor.Yellow);
                else
                    toolStripProgressBarDrive.SetColor(ProgressBarExtension.ProgressBarColor.Red);
                toolStripProgressBarDrive.ToolTipText = (toolStripProgressBarDrive.Maximum - toolStripProgressBarDrive.Value).ToString() + "% is free";

            }

            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                curDir = curDir.Parent;
                FillListView(curDir);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally { listViewBrowser.EndUpdate(); }
        }

        private Size GetSizeOfToolTipPopUp()
        {
            return TextRenderer.MeasureText(toolTipPopUpText, new Font("Arial", 16.0f));
        }
        #endregion

        #region ListView and ToolTip Event Handlers
        private void listViewBrowser_ItemActivate(object sender, EventArgs e)
        {
            toolStripTextBoxSearch.Text = "";
            foreach (ListViewItem item in ((ListView)sender).SelectedItems)
            {
                try
                {
                    if (item.ImageKey != "folder")
                        System.Diagnostics.Process.Start(item.Tag.ToString());
                    else
                        curDir = new DirectoryInfo(item.Tag.ToString());
                }
                catch (System.ComponentModel.Win32Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    continue;
                }
            }
            if (((ListView)sender).SelectedItems.Count == 0)
            {
                toolTipPopUpText = "Select file first!";
                toolTipPopUp.Show(toolTipPopUpText, this, (this.Width / 2 - GetSizeOfToolTipPopUp().Width / 2), (this.Height / 2 - GetSizeOfToolTipPopUp().Height / 2), 900);
            }
            FillListView(curDir);
        }

        private void listViewBrowser_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ascendingToolStripMenuItem.Checked = descendingToolStripMenuItem.Checked = false;

            if (e.Column != sortColumnNumber)
            {
                sortColumnNumber = e.Column;
                listViewBrowser.Sorting = SortOrder.Ascending;
            }
            else
            {
                if (listViewBrowser.Sorting == SortOrder.Ascending)
                    listViewBrowser.Sorting = SortOrder.Descending;
                else
                    listViewBrowser.Sorting = SortOrder.Ascending;
            }

            if ((e.Column == 0) && (listViewBrowser.Sorting == SortOrder.Ascending))
                ascendingToolStripMenuItem.Checked = true;
            if ((e.Column == 0) && (listViewBrowser.Sorting == SortOrder.Descending))
                descendingToolStripMenuItem.Checked = true;

            listViewBrowser.ListViewItemSorter = new ListViewItemComparer(e.Column, listViewBrowser.Sorting);

            if (listViewBrowser.SelectedItems.Count != 0)
                listViewBrowser.SelectedItems[0].EnsureVisible();
        }

        private void listViewBrowser_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            string toAdd = " Selected: ";
            if (toolStripStatusLabelCounts.Text.Contains(toAdd))
                toolStripStatusLabelCounts.Text = toolStripStatusLabelCounts.Text.Remove(toolStripStatusLabelCounts.Text.LastIndexOf(toAdd));
            if (listViewBrowser.SelectedItems.Count != 0)
                toolStripStatusLabelCounts.Text += toAdd + listViewBrowser.SelectedItems.Count.ToString();
            toolStripStatusLabelCounts.ToolTipText = toolStripStatusLabelCounts.Text;
        }

        private void toolTipPopUp_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.Graphics.FillRectangle(SystemBrushes.GradientInactiveCaption, e.Bounds);
            using (StringFormat sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Center;
                sf.FormatFlags = StringFormatFlags.NoWrap;
                sf.LineAlignment = StringAlignment.Center;
                using (Font f = new Font("Arial", 14.0f))
                using (Brush brush = new SolidBrush(ColorTranslator.FromHtml("#333333")))
                    e.Graphics.DrawString(e.ToolTipText, f, brush, e.Bounds, sf);
            }
        }

        private void toolTipPopUp_Popup(object sender, PopupEventArgs e)
        {
            e.ToolTipSize = TextRenderer.MeasureText(toolTipPopUpText, new Font("Arial", 16.0f));
        }
        #endregion

        #region Standard Panel Event Handlers
        private void toolStripButtonBack_Click(object sender, EventArgs e)
        {
            toolStripTextBoxSearch.Text = "";
            DirectoryInfo parentDir = curDir.Parent;
            if (parentDir != null)
            {
                FillListView(parentDir);
                curDir = parentDir;
            }
            else
            {
                toolTipPopUpText = "Root directory has been reached!";
                toolTipPopUp.Show(toolTipPopUpText, this, (this.Width / 2 - GetSizeOfToolTipPopUp().Width / 2), (this.Height / 2 - GetSizeOfToolTipPopUp().Height / 2), 900);
                FillListView(curDir.Root);
                curDir = curDir.Root;
            }
        }

        private void toolStripButtonView_Click(object sender, EventArgs e)
        {
            View curView = listViewBrowser.View;
            View[] views = { View.Details, View.List, View.SmallIcon };

            for (int i = 0; i < views.Length; i++)
            {
                if (curView == views[i])
                {
                    if (i == views.Length - 1)
                        curView = views[0];
                    else
                        curView = views[++i];
                    break;
                }
            }

            listViewBrowser.View = curView;

            detailsToolStripMenuItem.Checked = listToolStripMenuItem.Checked = smallIconToolStripMenuItem.Checked = false;
            if (curView == views[0])
                detailsToolStripMenuItem.Checked = true;
            if (curView == views[1])
                listToolStripMenuItem.Checked = true;
            if (curView == views[2])
                smallIconToolStripMenuItem.Checked = true;

            if (listViewBrowser.SelectedItems.Count != 0)
                listViewBrowser.SelectedItems[0].EnsureVisible();
        }

        private void toolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            toolStripTextBoxSearch.Text = "";
            FillListView(curDir);
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            if (listViewBrowser.SelectedItems.Count != 0)
            {
                ListViewItem[] items = new ListViewItem[listViewBrowser.SelectedItems.Count];
                listViewBrowser.SelectedItems.CopyTo(items, 0);
                (new FormProgress("Deleting files...", items, OperationOptions.Delete)).ShowDialog();
                toolStripButtonRefresh.PerformClick();
            }
            else
            {
                toolTipPopUpText = "Select file first!";
                toolTipPopUp.Show(toolTipPopUpText, this, (this.Width / 2 - GetSizeOfToolTipPopUp().Width / 2), (this.Height / 2 - GetSizeOfToolTipPopUp().Height / 2), 900);
            }
        }

        private void toolStripButtonCut_Click(object sender, EventArgs e)
        {
            if (listViewBrowser.SelectedItems.Count != 0)
            {
                Operations.ResetOperation();
                foreach (ListViewItem item in listViewBrowser.SelectedItems)
                    Operations.CutOrCopy((string)item.Tag, OperationOptions.Cut);
            }
            else
            {
                toolTipPopUpText = "Select file first!";
                toolTipPopUp.Show(toolTipPopUpText, this, (this.Width / 2 - GetSizeOfToolTipPopUp().Width / 2), (this.Height / 2 - GetSizeOfToolTipPopUp().Height / 2), 900);
            }
        }

        private void toolStripButtonCopy_Click(object sender, EventArgs e)
        {
            if (listViewBrowser.SelectedItems.Count != 0)
            {
                Operations.ResetOperation();
                foreach (ListViewItem item in listViewBrowser.SelectedItems)
                    Operations.CutOrCopy((string)item.Tag, OperationOptions.Copy);
            }
            else
            {
                toolTipPopUpText = "Select file first!";
                toolTipPopUp.Show(toolTipPopUpText, this, (this.Width / 2 - GetSizeOfToolTipPopUp().Width / 2), (this.Height / 2 - GetSizeOfToolTipPopUp().Height / 2), 900);
            }
        }

        private void toolStripButtonPaste_Click(object sender, EventArgs e)
        {
            if (Operations.FilesToPasteCount != 0)
            {
                (new FormProgress(caption : "Pasting files...", destination : curDir.FullName)).ShowDialog();
                toolStripButtonRefresh.PerformClick();
            }
            else
            {
                toolTipPopUpText = "Cut or Copy file first!";
                toolTipPopUp.Show(toolTipPopUpText, this, (this.Width / 2 - GetSizeOfToolTipPopUp().Width / 2), (this.Height / 2 - GetSizeOfToolTipPopUp().Height / 2), 900);
            }
        }

        private void toolStripComboBoxDrives_Click(object sender, EventArgs e)
        {
            FillDrives(new DriveInfo(curDir.Root.FullName));
        }

        private void toolStripComboBoxDrives_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (curDir.Root.FullName == ((ToolStripComboBox)sender).SelectedItem.ToString())
                return;

            curDir = new DirectoryInfo(((ToolStripComboBox)sender).SelectedItem.ToString()).Root;
            FillListView(curDir);
            listViewBrowser.Focus();
        }

        private void toolStripTextBoxSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string pattern = ((ToolStripTextBox)sender).Text;

                if (string.IsNullOrWhiteSpace(pattern))
                    return;

                foreach (ListViewItem item in listViewBrowser.Items)
                    item.Selected = false;

                listViewBrowser.MultiSelect = false;
                Regex search = new Regex(pattern, RegexOptions.IgnoreCase);
                for (int index = 0; index < listViewBrowser.Items.Count; index++)
                {
                    Match result = search.Match(listViewBrowser.Items[index].Text + listViewBrowser.Items[index].SubItems[1].Text);
                    if (result.Success)
                    {
                        listViewBrowser.Items[index].Selected = true;
                        listViewBrowser.Items[index].EnsureVisible();
                        break;
                    }
                }
            }
            catch (ArgumentException) { return; }
            finally { listViewBrowser.MultiSelect = true; }
        }

        private void toolStripTextBoxSearch_Enter(object sender, EventArgs e)
        {
            toolStripTextBoxSearch_TextChanged(toolStripTextBoxSearch, EventArgs.Empty);
        }

        private void toolStripTextBoxSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
                listViewBrowser.Focus();
        }
        #endregion

        #region Menu Panel Event Handlers
        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripTextBoxSearch.Focus();
        }

        private void statusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (statusStrip.Visible = statusBarToolStripMenuItem.Checked)
                this.listViewBrowser.Height -= 13;
            else
                this.listViewBrowser.Height += 13;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listViewBrowser.Items)
                item.Selected = true;
        }

        private void unselectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listViewBrowser.Items)
                item.Selected = false;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listViewBrowser_ItemActivate(listViewBrowser, EventArgs.Empty);
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonRefresh.PerformClick();
        }

        private void ascendingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            descendingToolStripMenuItem.Checked = false;
            ascendingToolStripMenuItem.Checked = true;
            listViewBrowser.ListViewItemSorter = new ListViewItemComparer(0, SortOrder.Ascending);
        }

        private void descendingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ascendingToolStripMenuItem.Checked = false;
            descendingToolStripMenuItem.Checked = true;
            listViewBrowser.ListViewItemSorter = new ListViewItemComparer(0, SortOrder.Descending);
        }

        private void detailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listToolStripMenuItem.Checked = smallIconToolStripMenuItem.Checked = false;
            detailsToolStripMenuItem.Checked = true;
            listViewBrowser.View = View.Details;
        }

        private void listToolStripMenuItem_Click(object sender, EventArgs e)
        {
            detailsToolStripMenuItem.Checked = smallIconToolStripMenuItem.Checked = false;
            listToolStripMenuItem.Checked = true;
            listViewBrowser.View = View.List;
        }

        private void smallIconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            detailsToolStripMenuItem.Checked = listToolStripMenuItem.Checked = false;
            smallIconToolStripMenuItem.Checked = true;
            listViewBrowser.View = View.SmallIcon;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string about = string.Format("Файловый менеджер \"C# Explorer\".\nНаписан с использованием библиотек .NET 4.5.\n" +
                                        "Copyright © 2014 Титов Н.А. (гр. МА-12-1).\nДля использования исключительно в учебных целях.");
            MessageBox.Show(about, "About \"C# Explorer\"", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FileSystemInfo selectedItem;

                if (listViewBrowser.SelectedItems.Count == 0)
                    selectedItem = curDir;
                else if (listViewBrowser.SelectedItems[0].ImageKey == "folder")
                    selectedItem = new DirectoryInfo(listViewBrowser.SelectedItems[0].Tag.ToString());
                else
                    selectedItem = new FileInfo(listViewBrowser.SelectedItems[0].Tag.ToString());

                FormProperties itemProperties = new FormProperties(selectedItem);
                itemProperties.ShowDialog();
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                toolStripButtonRefresh.PerformClick();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (listViewBrowser.SelectedItems.Count != 0)
                {
                    FormRenameOrCreate renameItem = new FormRenameOrCreate(FormOptions.Rename, (string)listViewBrowser.SelectedItems[0].Tag);
                    renameItem.ShowDialog();
                    toolStripButtonRefresh.PerformClick();
                }
                else
                {
                    toolTipPopUpText = "Select file first!";
                    toolTipPopUp.Show(toolTipPopUpText, this, (this.Width / 2 - GetSizeOfToolTipPopUp().Width / 2), (this.Height / 2 - GetSizeOfToolTipPopUp().Height / 2), 900);
                }
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                toolStripButtonRefresh.PerformClick();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void newFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FormRenameOrCreate renameItem = new FormRenameOrCreate(FormOptions.Create, curDir.FullName);
                renameItem.ShowDialog();
                toolStripButtonRefresh.PerformClick();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonDelete.PerformClick();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonCut.PerformClick();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonCopy.PerformClick();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonPaste.PerformClick();
        }

        private void hiddenFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonRefresh.PerformClick();
        }

        private void systemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonRefresh.PerformClick();
        }
        #endregion

        #region Context Menu Event Handlers
        private void contextMenuStripRidhtClick_Opening(object sender, CancelEventArgs e)
        {
            ContextMenuStrip contextMenu = sender as ContextMenuStrip;
            bool anyItemSelected = listViewBrowser.SelectedItems.Count != 0;

            contextMenu.Items[1].Visible =
                contextMenu.Items[3].Visible =
                contextMenu.Items[4].Visible =
                contextMenu.Items[6].Visible =
                contextMenu.Items[7].Visible =
                contextMenu.Items[8].Visible = anyItemSelected;
            contextMenu.Items[0].Visible =
            contextMenu.Items[2].Visible =
            contextMenu.Items[5].Visible = !anyItemSelected;
        }

        private void newFolderToolStripContextMenuItem_Click(object sender, EventArgs e)
        {
            newFolderToolStripMenuItem.PerformClick();
        }

        private void openToolStripContextMenuItem_Click(object sender, EventArgs e)
        {
            listViewBrowser_ItemActivate(listViewBrowser, EventArgs.Empty);
        }

        private void cutToolStripContextMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonCut.PerformClick();
        }

        private void copyToolStripContextMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonCopy.PerformClick();
        }

        private void pasteToolStripContextMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonPaste.PerformClick();
        }

        private void deleteToolStripContextMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButtonDelete.PerformClick();
        }

        private void renameToolStripContextMenuItem_Click(object sender, EventArgs e)
        {
            renameToolStripMenuItem.PerformClick();
        }

        private void propertiesToolContextStripMenuItem_Click(object sender, EventArgs e)
        {
            propertiesToolStripMenuItem.PerformClick();
        }
        #endregion
    }
}
