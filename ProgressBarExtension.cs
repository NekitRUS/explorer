﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Lab1
{
    static class ProgressBarExtension
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);

        public enum ProgressBarColor { Green = 1, Red, Yellow };

        public static void SetColor(this ToolStripProgressBar pBar, ProgressBarColor color)
        {
            SendMessage(pBar.ProgressBar.Handle, 1040, (IntPtr)color, IntPtr.Zero);
        }
    }
}
