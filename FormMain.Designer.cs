﻿namespace Lab1
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.listViewBrowser = new System.Windows.Forms.ListView();
            this.contextMenuStripRidhtClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newFolderToolStripContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripContextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.propertiesToolContextStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBoxDrive = new System.Windows.Forms.PictureBox();
            this.pictureBoxFind = new System.Windows.Forms.PictureBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelPath = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelCounts = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelDrive = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBarDrive = new System.Windows.Forms.ToolStripProgressBar();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.newFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unselectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currentViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smallIconToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.sortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ascendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hiddenFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStandard = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonBack = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCut = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPaste = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonView = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBoxSearch = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripComboBoxDrives = new System.Windows.Forms.ToolStripComboBox();
            this.toolTipPopUp = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStripRidhtClick.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFind)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.toolStripStandard.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewBrowser
            // 
            this.listViewBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewBrowser.ContextMenuStrip = this.contextMenuStripRidhtClick;
            this.listViewBrowser.FullRowSelect = true;
            this.listViewBrowser.HideSelection = false;
            this.listViewBrowser.Location = new System.Drawing.Point(12, 73);
            this.listViewBrowser.Name = "listViewBrowser";
            this.listViewBrowser.Size = new System.Drawing.Size(840, 304);
            this.listViewBrowser.SmallImageList = this.imageList;
            this.listViewBrowser.TabIndex = 0;
            this.listViewBrowser.UseCompatibleStateImageBehavior = false;
            this.listViewBrowser.View = System.Windows.Forms.View.Details;
            this.listViewBrowser.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewBrowser_ColumnClick);
            this.listViewBrowser.ItemActivate += new System.EventHandler(this.listViewBrowser_ItemActivate);
            this.listViewBrowser.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listViewBrowser_ItemSelectionChanged);
            // 
            // contextMenuStripRidhtClick
            // 
            this.contextMenuStripRidhtClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFolderToolStripContextMenuItem,
            this.openToolStripContextMenuItem,
            this.toolStripSeparator12,
            this.cutToolStripContextMenuItem,
            this.copyToolStripContextMenuItem,
            this.pasteToolStripContextMenuItem,
            this.toolStripSeparator13,
            this.deleteToolStripContextMenuItem,
            this.renameToolStripContextMenuItem,
            this.toolStripSeparator14,
            this.propertiesToolContextStripMenuItem});
            this.contextMenuStripRidhtClick.Name = "contextMenuStripRidhtClick";
            this.contextMenuStripRidhtClick.Size = new System.Drawing.Size(128, 198);
            this.contextMenuStripRidhtClick.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripRidhtClick_Opening);
            // 
            // newFolderToolStripContextMenuItem
            // 
            this.newFolderToolStripContextMenuItem.Image = global::Lab1.Properties.Resources.new_folder;
            this.newFolderToolStripContextMenuItem.Name = "newFolderToolStripContextMenuItem";
            this.newFolderToolStripContextMenuItem.ShowShortcutKeys = false;
            this.newFolderToolStripContextMenuItem.Size = new System.Drawing.Size(127, 22);
            this.newFolderToolStripContextMenuItem.Text = "New Folder";
            this.newFolderToolStripContextMenuItem.Click += new System.EventHandler(this.newFolderToolStripContextMenuItem_Click);
            // 
            // openToolStripContextMenuItem
            // 
            this.openToolStripContextMenuItem.Image = global::Lab1.Properties.Resources.open;
            this.openToolStripContextMenuItem.Name = "openToolStripContextMenuItem";
            this.openToolStripContextMenuItem.ShowShortcutKeys = false;
            this.openToolStripContextMenuItem.Size = new System.Drawing.Size(127, 22);
            this.openToolStripContextMenuItem.Text = "Open";
            this.openToolStripContextMenuItem.Click += new System.EventHandler(this.openToolStripContextMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(124, 6);
            // 
            // cutToolStripContextMenuItem
            // 
            this.cutToolStripContextMenuItem.Image = global::Lab1.Properties.Resources.cut;
            this.cutToolStripContextMenuItem.Name = "cutToolStripContextMenuItem";
            this.cutToolStripContextMenuItem.ShowShortcutKeys = false;
            this.cutToolStripContextMenuItem.Size = new System.Drawing.Size(127, 22);
            this.cutToolStripContextMenuItem.Text = "Cut";
            this.cutToolStripContextMenuItem.Click += new System.EventHandler(this.cutToolStripContextMenuItem_Click);
            // 
            // copyToolStripContextMenuItem
            // 
            this.copyToolStripContextMenuItem.Image = global::Lab1.Properties.Resources.copy;
            this.copyToolStripContextMenuItem.Name = "copyToolStripContextMenuItem";
            this.copyToolStripContextMenuItem.ShowShortcutKeys = false;
            this.copyToolStripContextMenuItem.Size = new System.Drawing.Size(127, 22);
            this.copyToolStripContextMenuItem.Text = "Copy";
            this.copyToolStripContextMenuItem.Click += new System.EventHandler(this.copyToolStripContextMenuItem_Click);
            // 
            // pasteToolStripContextMenuItem
            // 
            this.pasteToolStripContextMenuItem.Image = global::Lab1.Properties.Resources.paste;
            this.pasteToolStripContextMenuItem.Name = "pasteToolStripContextMenuItem";
            this.pasteToolStripContextMenuItem.ShowShortcutKeys = false;
            this.pasteToolStripContextMenuItem.Size = new System.Drawing.Size(127, 22);
            this.pasteToolStripContextMenuItem.Text = "Paste";
            this.pasteToolStripContextMenuItem.Click += new System.EventHandler(this.pasteToolStripContextMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(124, 6);
            // 
            // deleteToolStripContextMenuItem
            // 
            this.deleteToolStripContextMenuItem.Image = global::Lab1.Properties.Resources.delete;
            this.deleteToolStripContextMenuItem.Name = "deleteToolStripContextMenuItem";
            this.deleteToolStripContextMenuItem.ShowShortcutKeys = false;
            this.deleteToolStripContextMenuItem.Size = new System.Drawing.Size(127, 22);
            this.deleteToolStripContextMenuItem.Text = "Delete";
            this.deleteToolStripContextMenuItem.Click += new System.EventHandler(this.deleteToolStripContextMenuItem_Click);
            // 
            // renameToolStripContextMenuItem
            // 
            this.renameToolStripContextMenuItem.Image = global::Lab1.Properties.Resources.rename;
            this.renameToolStripContextMenuItem.Name = "renameToolStripContextMenuItem";
            this.renameToolStripContextMenuItem.ShowShortcutKeys = false;
            this.renameToolStripContextMenuItem.Size = new System.Drawing.Size(127, 22);
            this.renameToolStripContextMenuItem.Text = "Rename";
            this.renameToolStripContextMenuItem.Click += new System.EventHandler(this.renameToolStripContextMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(124, 6);
            // 
            // propertiesToolContextStripMenuItem
            // 
            this.propertiesToolContextStripMenuItem.Image = global::Lab1.Properties.Resources.properties;
            this.propertiesToolContextStripMenuItem.Name = "propertiesToolContextStripMenuItem";
            this.propertiesToolContextStripMenuItem.ShowShortcutKeys = false;
            this.propertiesToolContextStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.propertiesToolContextStripMenuItem.Text = "Properties";
            this.propertiesToolContextStripMenuItem.Click += new System.EventHandler(this.propertiesToolContextStripMenuItem_Click);
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(32, 32);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // pictureBoxDrive
            // 
            this.pictureBoxDrive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxDrive.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pictureBoxDrive.Image = global::Lab1.Properties.Resources.drive;
            this.pictureBoxDrive.Location = new System.Drawing.Point(576, 24);
            this.pictureBoxDrive.Name = "pictureBoxDrive";
            this.pictureBoxDrive.Size = new System.Drawing.Size(24, 42);
            this.pictureBoxDrive.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxDrive.TabIndex = 8;
            this.pictureBoxDrive.TabStop = false;
            // 
            // pictureBoxFind
            // 
            this.pictureBoxFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxFind.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pictureBoxFind.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFind.Image")));
            this.pictureBoxFind.Location = new System.Drawing.Point(677, 24);
            this.pictureBoxFind.Name = "pictureBoxFind";
            this.pictureBoxFind.Size = new System.Drawing.Size(22, 42);
            this.pictureBoxFind.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxFind.TabIndex = 10;
            this.pictureBoxFind.TabStop = false;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelPath,
            this.toolStripStatusLabelCounts,
            this.toolStripStatusLabelDrive,
            this.toolStripProgressBarDrive});
            this.statusStrip.Location = new System.Drawing.Point(0, 380);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.ShowItemToolTips = true;
            this.statusStrip.Size = new System.Drawing.Size(864, 22);
            this.statusStrip.TabIndex = 12;
            this.statusStrip.Text = "statusStrip";
            // 
            // toolStripStatusLabelPath
            // 
            this.toolStripStatusLabelPath.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabelPath.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripStatusLabelPath.Name = "toolStripStatusLabelPath";
            this.toolStripStatusLabelPath.Size = new System.Drawing.Size(265, 17);
            this.toolStripStatusLabelPath.Spring = true;
            this.toolStripStatusLabelPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabelCounts
            // 
            this.toolStripStatusLabelCounts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabelCounts.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripStatusLabelCounts.Name = "toolStripStatusLabelCounts";
            this.toolStripStatusLabelCounts.Size = new System.Drawing.Size(265, 17);
            this.toolStripStatusLabelCounts.Spring = true;
            // 
            // toolStripStatusLabelDrive
            // 
            this.toolStripStatusLabelDrive.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabelDrive.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripStatusLabelDrive.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripStatusLabelDrive.Name = "toolStripStatusLabelDrive";
            this.toolStripStatusLabelDrive.Size = new System.Drawing.Size(265, 17);
            this.toolStripStatusLabelDrive.Spring = true;
            this.toolStripStatusLabelDrive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStripProgressBarDrive
            // 
            this.toolStripProgressBarDrive.Name = "toolStripProgressBarDrive";
            this.toolStripProgressBarDrive.Size = new System.Drawing.Size(50, 16);
            this.toolStripProgressBarDrive.Step = 1;
            this.toolStripProgressBarDrive.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(864, 24);
            this.menuStripMain.TabIndex = 1;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.propertiesToolStripMenuItem,
            this.toolStripSeparator4,
            this.newFolderToolStripMenuItem,
            this.toolStripSeparator11,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = global::Lab1.Properties.Resources.open;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Image = global::Lab1.Properties.Resources.properties;
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            this.propertiesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.propertiesToolStripMenuItem.Text = "&Properties";
            this.propertiesToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(174, 6);
            // 
            // newFolderToolStripMenuItem
            // 
            this.newFolderToolStripMenuItem.Image = global::Lab1.Properties.Resources.new_folder;
            this.newFolderToolStripMenuItem.Name = "newFolderToolStripMenuItem";
            this.newFolderToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newFolderToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.newFolderToolStripMenuItem.Text = "New Folder";
            this.newFolderToolStripMenuItem.Click += new System.EventHandler(this.newFolderToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(174, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::Lab1.Properties.Resources.exit;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.X)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renameToolStripMenuItem,
            this.toolStripSeparator5,
            this.deleteToolStripMenuItem,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator10,
            this.findToolStripMenuItem,
            this.toolStripSeparator6,
            this.selectAllToolStripMenuItem,
            this.unselectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Image = global::Lab1.Properties.Resources.rename;
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.renameToolStripMenuItem.Text = "Re&name";
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(161, 6);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Image = global::Lab1.Properties.Resources.delete;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = global::Lab1.Properties.Resources.cut;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cutToolStripMenuItem.Text = "C&ut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = global::Lab1.Properties.Resources.copy;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.copyToolStripMenuItem.Text = "Cop&y";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = global::Lab1.Properties.Resources.paste;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.pasteToolStripMenuItem.Text = "Pas&te";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(161, 6);
            // 
            // findToolStripMenuItem
            // 
            this.findToolStripMenuItem.Image = global::Lab1.Properties.Resources.find;
            this.findToolStripMenuItem.Name = "findToolStripMenuItem";
            this.findToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.findToolStripMenuItem.Text = "Find";
            this.findToolStripMenuItem.Click += new System.EventHandler(this.findToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(161, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Image = global::Lab1.Properties.Resources.select_all;
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.selectAllToolStripMenuItem.Text = "&Select All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // unselectAllToolStripMenuItem
            // 
            this.unselectAllToolStripMenuItem.Image = global::Lab1.Properties.Resources.deselect;
            this.unselectAllToolStripMenuItem.Name = "unselectAllToolStripMenuItem";
            this.unselectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.unselectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.unselectAllToolStripMenuItem.Text = "Deselect";
            this.unselectAllToolStripMenuItem.Click += new System.EventHandler(this.unselectAllToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.currentViewToolStripMenuItem,
            this.toolStripSeparator7,
            this.sortToolStripMenuItem,
            this.toolStripSeparator8,
            this.showToolStripMenuItem,
            this.toolStripSeparator15,
            this.statusBarToolStripMenuItem,
            this.toolStripSeparator9,
            this.refreshToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // currentViewToolStripMenuItem
            // 
            this.currentViewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detailsToolStripMenuItem,
            this.listToolStripMenuItem,
            this.smallIconToolStripMenuItem});
            this.currentViewToolStripMenuItem.Name = "currentViewToolStripMenuItem";
            this.currentViewToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.currentViewToolStripMenuItem.Text = "Current Vie&w";
            // 
            // detailsToolStripMenuItem
            // 
            this.detailsToolStripMenuItem.Checked = true;
            this.detailsToolStripMenuItem.CheckOnClick = true;
            this.detailsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.detailsToolStripMenuItem.Image = global::Lab1.Properties.Resources.details;
            this.detailsToolStripMenuItem.Name = "detailsToolStripMenuItem";
            this.detailsToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.detailsToolStripMenuItem.Text = "Details";
            this.detailsToolStripMenuItem.Click += new System.EventHandler(this.detailsToolStripMenuItem_Click);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.CheckOnClick = true;
            this.listToolStripMenuItem.Image = global::Lab1.Properties.Resources.list;
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.listToolStripMenuItem.Text = "&List";
            this.listToolStripMenuItem.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // smallIconToolStripMenuItem
            // 
            this.smallIconToolStripMenuItem.CheckOnClick = true;
            this.smallIconToolStripMenuItem.Image = global::Lab1.Properties.Resources.small_icons;
            this.smallIconToolStripMenuItem.Name = "smallIconToolStripMenuItem";
            this.smallIconToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.smallIconToolStripMenuItem.Text = "S&mall Icon";
            this.smallIconToolStripMenuItem.Click += new System.EventHandler(this.smallIconToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(139, 6);
            // 
            // sortToolStripMenuItem
            // 
            this.sortToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ascendingToolStripMenuItem,
            this.descendingToolStripMenuItem});
            this.sortToolStripMenuItem.Name = "sortToolStripMenuItem";
            this.sortToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.sortToolStripMenuItem.Text = "Sort";
            // 
            // ascendingToolStripMenuItem
            // 
            this.ascendingToolStripMenuItem.CheckOnClick = true;
            this.ascendingToolStripMenuItem.Image = global::Lab1.Properties.Resources.ascending;
            this.ascendingToolStripMenuItem.Name = "ascendingToolStripMenuItem";
            this.ascendingToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.ascendingToolStripMenuItem.Text = "As&cending";
            this.ascendingToolStripMenuItem.Click += new System.EventHandler(this.ascendingToolStripMenuItem_Click);
            // 
            // descendingToolStripMenuItem
            // 
            this.descendingToolStripMenuItem.CheckOnClick = true;
            this.descendingToolStripMenuItem.Image = global::Lab1.Properties.Resources.descending;
            this.descendingToolStripMenuItem.Name = "descendingToolStripMenuItem";
            this.descendingToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.descendingToolStripMenuItem.Text = "Descend&ing";
            this.descendingToolStripMenuItem.Click += new System.EventHandler(this.descendingToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(139, 6);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hiddenFilesToolStripMenuItem,
            this.systemToolStripMenuItem});
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.showToolStripMenuItem.Text = "Show Files";
            // 
            // hiddenFilesToolStripMenuItem
            // 
            this.hiddenFilesToolStripMenuItem.CheckOnClick = true;
            this.hiddenFilesToolStripMenuItem.Image = global::Lab1.Properties.Resources.hidden;
            this.hiddenFilesToolStripMenuItem.Name = "hiddenFilesToolStripMenuItem";
            this.hiddenFilesToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.hiddenFilesToolStripMenuItem.Text = "Hidden";
            this.hiddenFilesToolStripMenuItem.Click += new System.EventHandler(this.hiddenFilesToolStripMenuItem_Click);
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.CheckOnClick = true;
            this.systemToolStripMenuItem.Image = global::Lab1.Properties.Resources.system;
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.systemToolStripMenuItem.Text = "System";
            this.systemToolStripMenuItem.Click += new System.EventHandler(this.systemToolStripMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(139, 6);
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckOnClick = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.statusBarToolStripMenuItem.Text = "Status &Bar";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.statusBarToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(139, 6);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Image = global::Lab1.Properties.Resources.refresh;
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.refreshToolStripMenuItem.Text = "&Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = global::Lab1.Properties.Resources.info;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStripStandard
            // 
            this.toolStripStandard.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.toolStripStandard.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonBack,
            this.toolStripButtonRefresh,
            this.toolStripSeparator1,
            this.toolStripButtonDelete,
            this.toolStripButtonCut,
            this.toolStripButtonCopy,
            this.toolStripButtonPaste,
            this.toolStripSeparator2,
            this.toolStripButtonView,
            this.toolStripTextBoxSearch,
            this.toolStripSeparator3,
            this.toolStripComboBoxDrives});
            this.toolStripStandard.Location = new System.Drawing.Point(0, 24);
            this.toolStripStandard.Name = "toolStripStandard";
            this.toolStripStandard.Size = new System.Drawing.Size(864, 43);
            this.toolStripStandard.TabIndex = 2;
            // 
            // toolStripButtonBack
            // 
            this.toolStripButtonBack.AutoSize = false;
            this.toolStripButtonBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonBack.Image = global::Lab1.Properties.Resources.back;
            this.toolStripButtonBack.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBack.Name = "toolStripButtonBack";
            this.toolStripButtonBack.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonBack.ToolTipText = "Назад";
            this.toolStripButtonBack.Click += new System.EventHandler(this.toolStripButtonBack_Click);
            // 
            // toolStripButtonRefresh
            // 
            this.toolStripButtonRefresh.AutoSize = false;
            this.toolStripButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRefresh.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRefresh.Image")));
            this.toolStripButtonRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
            this.toolStripButtonRefresh.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonRefresh.ToolTipText = "Обновить";
            this.toolStripButtonRefresh.Click += new System.EventHandler(this.toolStripButtonRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 43);
            // 
            // toolStripButtonDelete
            // 
            this.toolStripButtonDelete.AutoSize = false;
            this.toolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDelete.Image = global::Lab1.Properties.Resources.delete;
            this.toolStripButtonDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDelete.Name = "toolStripButtonDelete";
            this.toolStripButtonDelete.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonDelete.ToolTipText = "Удалить";
            this.toolStripButtonDelete.Click += new System.EventHandler(this.toolStripButtonDelete_Click);
            // 
            // toolStripButtonCut
            // 
            this.toolStripButtonCut.AutoSize = false;
            this.toolStripButtonCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCut.Image = global::Lab1.Properties.Resources.cut;
            this.toolStripButtonCut.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCut.Name = "toolStripButtonCut";
            this.toolStripButtonCut.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonCut.ToolTipText = "Вырезать";
            this.toolStripButtonCut.Click += new System.EventHandler(this.toolStripButtonCut_Click);
            // 
            // toolStripButtonCopy
            // 
            this.toolStripButtonCopy.AutoSize = false;
            this.toolStripButtonCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCopy.Image = global::Lab1.Properties.Resources.copy;
            this.toolStripButtonCopy.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCopy.Name = "toolStripButtonCopy";
            this.toolStripButtonCopy.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonCopy.ToolTipText = "Копировать";
            this.toolStripButtonCopy.Click += new System.EventHandler(this.toolStripButtonCopy_Click);
            // 
            // toolStripButtonPaste
            // 
            this.toolStripButtonPaste.AutoSize = false;
            this.toolStripButtonPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPaste.Image = global::Lab1.Properties.Resources.paste;
            this.toolStripButtonPaste.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPaste.Name = "toolStripButtonPaste";
            this.toolStripButtonPaste.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonPaste.ToolTipText = "Вставить";
            this.toolStripButtonPaste.Click += new System.EventHandler(this.toolStripButtonPaste_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 43);
            // 
            // toolStripButtonView
            // 
            this.toolStripButtonView.AutoSize = false;
            this.toolStripButtonView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonView.Image = global::Lab1.Properties.Resources.view;
            this.toolStripButtonView.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonView.Name = "toolStripButtonView";
            this.toolStripButtonView.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonView.ToolTipText = "Изменить представление";
            this.toolStripButtonView.Click += new System.EventHandler(this.toolStripButtonView_Click);
            // 
            // toolStripTextBoxSearch
            // 
            this.toolStripTextBoxSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripTextBoxSearch.AutoSize = false;
            this.toolStripTextBoxSearch.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripTextBoxSearch.Margin = new System.Windows.Forms.Padding(28, 0, 11, 0);
            this.toolStripTextBoxSearch.Name = "toolStripTextBoxSearch";
            this.toolStripTextBoxSearch.Size = new System.Drawing.Size(150, 43);
            this.toolStripTextBoxSearch.ToolTipText = "Поиск в текущем каталоге\r\nПоддерживает регулярные выражения";
            this.toolStripTextBoxSearch.Enter += new System.EventHandler(this.toolStripTextBoxSearch_Enter);
            this.toolStripTextBoxSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBoxSearch_KeyPress);
            this.toolStripTextBoxSearch.TextChanged += new System.EventHandler(this.toolStripTextBoxSearch_TextChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 43);
            // 
            // toolStripComboBoxDrives
            // 
            this.toolStripComboBoxDrives.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripComboBoxDrives.AutoSize = false;
            this.toolStripComboBoxDrives.DropDownHeight = 95;
            this.toolStripComboBoxDrives.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxDrives.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripComboBoxDrives.IntegralHeight = false;
            this.toolStripComboBoxDrives.Name = "toolStripComboBoxDrives";
            this.toolStripComboBoxDrives.Size = new System.Drawing.Size(65, 23);
            this.toolStripComboBoxDrives.Sorted = true;
            this.toolStripComboBoxDrives.ToolTipText = "Выбор диска";
            this.toolStripComboBoxDrives.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxDrives_SelectedIndexChanged);
            this.toolStripComboBoxDrives.Click += new System.EventHandler(this.toolStripComboBoxDrives_Click);
            // 
            // toolTipPopUp
            // 
            this.toolTipPopUp.OwnerDraw = true;
            this.toolTipPopUp.ShowAlways = true;
            this.toolTipPopUp.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.toolTipPopUp_Draw);
            this.toolTipPopUp.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTipPopUp_Popup);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 402);
            this.Controls.Add(this.pictureBoxFind);
            this.Controls.Add(this.pictureBoxDrive);
            this.Controls.Add(this.toolStripStandard);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStripMain);
            this.Controls.Add(this.listViewBrowser);
            this.Icon = global::Lab1.Properties.Resources.favicon;
            this.MainMenuStrip = this.menuStripMain;
            this.MinimumSize = new System.Drawing.Size(880, 440);
            this.Name = "FormMain";
            this.Text = "C# Explorer";
            this.toolTip.SetToolTip(this, "Поиск в текущем каталоге\r\nывапвап");
            this.contextMenuStripRidhtClick.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDrive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFind)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.toolStripStandard.ResumeLayout(false);
            this.toolStripStandard.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewBrowser;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.PictureBox pictureBoxDrive;
        private System.Windows.Forms.PictureBox pictureBoxFind;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelPath;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelCounts;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelDrive;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarDrive;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStripStandard;
        private System.Windows.Forms.ToolStripButton toolStripButtonBack;
        private System.Windows.Forms.ToolStripButton toolStripButtonRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonDelete;
        private System.Windows.Forms.ToolStripButton toolStripButtonCut;
        private System.Windows.Forms.ToolStripButton toolStripButtonPaste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonView;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxDrives;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxSearch;
        private System.Windows.Forms.ToolStripButton toolStripButtonCopy;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unselectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currentViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem sortToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smallIconToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ascendingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem descendingToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolTip toolTipPopUp;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripRidhtClick;
        private System.Windows.Forms.ToolStripMenuItem openToolStripContextMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripContextMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripContextMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripContextMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripContextMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripContextMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolContextStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newFolderToolStripContextMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hiddenFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
    }
}

